# File: Makefile for Beyond the Titanic
# Location: 
# Author: bgstack15
# Startdate: 2019-03-31
# Title: Makefile for Beyond the Titanic source package
# Purpose: To use traditional Unix make utility
# History:
# Usage:
# Reference:
#    bgscripts Makefile, move-to-next-monitor Makefile
# Improve:
# Document:
# Dependencies:
#   Free Pascal compiler: fpc

APPNAME    = beyond-the-titanic
APPVERSION = 0.0.1
PKGDIR	  = $(CURDIR)
SRCDIR     = $(CURDIR)/src
prefix     = /usr
BINDIR     = $(DESTDIR)$(prefix)/bin
SHAREDIR	  = $(DESTDIR)$(prefix)/share
APPDIR     = $(SHAREDIR)/$(APPNAME)
APPSDIR	  = $(SHAREDIR)/applications
DOCDIR	  = $(SHAREDIR)/doc/$(APPNAME)
LICENSEDIR = $(SHAREDIR)/licenses/$(APPNAME)

awkbin     :=$(shell which awk)
cpbin      :=$(shell which cp)
echobin    :=$(shell which echo)
findbin    :=$(shell which find)
fpcbin	  :=$(shell which fpc)
grepbin    :=$(shell which grep)
installbin :=$(shell which install)
rmbin      :=$(shell which rm)
sedbin     :=$(shell which sed)
sortbin    :=$(shell which sort)
truebin    :=$(shell which true)

all:
	${fpcbin} $(SRCDIR)/BEYOND.PAS

.PHONY: clean install install_files uninstall list

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | ${awkbin} -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | ${sortbin} | ${grepbin} -E -v -e '^[^[:alnum:]]' -e '^$@$$'

install: install_files install_docs

install_files: 
	${echobin} Installing files to ${DESTDIR}
	${installbin} -d ${BINDIR} ${APPDIR} ${APPSDIR}
	ls -l ${SRCDIR}
	for word in $$( ${findbin} ${SRCDIR} ! -type d ! -name '*.*' -print ) ; do ${installbin} -m0644 $${word} ${APPDIR} ; done
	${installbin} -m '0755' ${SRCDIR}/BEYOND ${APPDIR}/
	${installbin} -m '0755' ${PKGDIR}/${APPNAME} ${BINDIR}/
	${installbin} -m '0644' ${PKGDIR}/${APPNAME}.desktop ${APPSDIR}/

install_docs:
	${echobin} Installing docs to ${DOCDIR}
	${installbin} -d ${DOCDIR} ${LICENSEDIR}
	${cpbin} -pr ${PKGDIR}/History/* ${PKGDIR}/INSTRUCT.TXT ${DOCDIR}
	${installbin} -m '0644' -t ${LICENSEDIR} ${PKGDIR}/*gpl*.txt ${PKGDIR}/LICENSES.txt

uninstall:
	${echobin} SRCDIR=${SRCDIR}
	${rmbin} -rf ${APPDIR}/* ${DOCDIR}/* ${LICENSEDIR}/* ${APPSDIR}/${APPNAME}.desktop ${BINDIR}/${APPNAME}

	# remove all installed directories that are now blank.
	for word in $$( ${findbin} ${DESTDIR} -mindepth 1 -type d -printf '%p\n' | ${awkbin} '{print length, $$0 }' | ${sortbin} -rn | ${awkbin} '{print $$2}' ) ; do ${findbin} $${word} -mindepth 1 1>/dev/null 2>&1 | read 1>/dev/null 2>&1 || { rmdir "$${word}" 2>/dev/null || ${truebin} ; } ; done

clean:
#${echobin} "target $@ not implemented yet! Gotta say unh."
	${rmbin} -f ${SRCDIR}/BEYOND ${SRCDIR}/BEYOND.o
