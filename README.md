# README for Beyond the Titanic
This is the bgstack15 release of Beyond the Titanic.


# TODO
* add directory paths to ROOMS1 files so the binary does not have to be in the same directory
* or, alternatively, use /usr/bin/BEYOND as a shell script to cd to /usr/share/beyond-the-titanic and call the binary that way.
* investigate how savegames are stored. can they be saved to ~/.config/beyond-the-titanic/ ?
* investigate compiling the game on the fly with terminal size you are currently running. (tput cols, tput lines)
