Original URL: https://gamefaqs.gamespot.com/pc/998038-beyond-the-titanic/faqs/76931
Beyond the Titanic Walkthrough by A.De Lisle (rad@crl.com)

[Added notes by Zizz. I felt it was important because the original Walkthrough 
is missing some crucial information and needs a bit of clarity. Also I suspect 
there's been minor but notable changes to different version of the game. Yes, 
I am adding notes to a walkthrough for a 35 year old game. I have obessions. 
Don't judge. -Zizz, March 2019]

Game from Apogee is no longer sold or supported.
[It was made freeware in 1998. Apogee is called 3DRealms now.]
 
Note:  There is a shareware version that is defective, but still 
playable.  The defect is in the bell over the bunk.  Don't press it.  
This does not do anything anyway.  Registered versions were correct.
[More Notes: There are hunger and light source turn limits in the game. If 
you're figuring things out yourself, once you figured ou the puzzles, it's best 
to go back to a previous save (or saved state) and do things as efficiently as 
possible. You will eventually reach a point where these limits do not matter
and you can stop worrying about wasting time/turns. Although the help file talks
about a multi word parser, it's best to simplify commands down to the fewest 
words possible just to not confuse the game.]
 
Aboard the boat:
Explore the boat, get the knife at Rear Deck.               10
Lifeboat: cut the rope, get and wear the harness.           25=35
          open the kit, examine kit, get all, 
          examine all items.                                25=60
[Don't need to GET the harness, just wear it]
 
Huge Cavern:
Cave of Pins:  turn on flashlight, go SE.
[If you lose your light source in the cave, you'll have no choice but to 
restart/reload]
Squeaky Cave: examine the cave markings -> message, go NW. E, SE
              NEPO EGASSAP is code (read backwards)
Hex Cave: examine the walls, SAY NEPO EGASSAP -> open wall   35=95
          open banana and eat it, drop banana peel and knife 10=105
[The banana gets you several more turns without dying of hunger, but you will 
eventually need another food source. The PEEL is useless.]
          tie rope to flashlight, drop flashlight
          CRAWL SW 
[You can only crawl into SHALLOW CLEFT while holding the ROPE and the STAKE. Not
hing else.]
Shallow Cleft: pull rope, get flashlight, get stake     40=145
          CRAWL NE (using rope to keep items)
Note: if you go NE to Sloppy Cave and N to Chasm, you die.
      The Ocean Bottom is another trap.
      Go East from Hex Cave to Winding Tunnel, then NW to Chasm.
Chasm:  Tie rope to flashlight, drop flashlight into chasm
      (can't see the bottom), put stake in ground, tie rope to stake,
      drop rope in chasm, climb down, let go (you will be hurt)
[Don't need to tie the rope to the flashlight or anything. Just PUT STAKE IN 
GROUND, UNTIE ROPE (Free it from flashlight, if it's not already), TIE ROPE TO 
STAKE, DROP ROPE IN CHASM, D, LET GO (Everything you're carrying including the 
flashlight will fall into CHASM BOTTOM along with you). Also, there is no going 
back at this point.]
End of Hope:  get flashlight then turn on flashlight   60=205
      (your left hand and right arm will be broken)
Chasm bottom:  SE -> Etched Stairs, NW -> Tiny Opening
     Etched Stairs NE -> Zoo                          10=215
[From CHASM BOTTOM, E -> TINY OPENING, NE -> EDGE OF SAUCER, U -> TOP OF SAUCER,
 The zoo and the blue fur is optional]
Zoo:  examine cage then examine fur, SW -> Stairs     15=230
Tiny Opening: NE -> Edge, NE -> Side, Climb up to top
Top:  open hatch then enter
Airlock:  Flashlight is dim, D, turn off flashlight
[You don't need to go back to the caves at this point, no need to worry about li
ght sources any more.]
 
      _______________________        26 = Central (N,S,E,W,NW,NE,SW,SE)
      |  34   |  27  |  28  | tube   27 = Ship's System   30=260pts
      |_______|______|______|            YELL AT COMPUTER -> 4 screens
      |  33   |  26  |  29  |            read screens for clues
      |_______|______|______|        33 = Bridge  
      |  32   |  31  |  30  |            Get suit and wear suit
      |_______|______|______|            25=285pts 
[At SHIP'S SYSTEMS: YELL (Wake up computer. Alternately: SAY HELLO), TYPE 4 (Any
number from 1-4), READ SCREEN ) The only important screen is #4, it tells you
the year you need to visit: 2171, and the Time Chamber panel combo, a 
RANDOMLY GENERATED 4 DIGIT NUMBER.]            
                                                          
      30 = Life Support
           Take off suit, drop suit                5 = 290               
           Get bottle, get pill, eat pill         15 = 305
[That doesn't work for me: GET BOTTLE, BREAK BOTTLE, GET PILL, EAT PILL. This 
eliminates the hunger turn limit. Or at least gives you tons of time.]
      31 = Supply 
           Examine shelves, get gun               15 = 320
      28 = Lab                                              
           Tube is for healing, enter tube  
           throw something at green button twice  55 = 375
[eg: THROW FLASHLIGHT AT BUTTON. Doesn't need to be the same item. I would do 
this as soon as possible so you can carry more things.]
      32 = Sleep Chamber
           Button over bed -> unusual sounds (registered only)
      34 = Engine Room
           Notice large red button, but use at end of game.
[Our 1912 protagonist recognizes radiation when he/she sees it. Pretty good job 
there, protagonist. *Wink* ]
      Central D -> Lower Entry and Cargo Rooms
      Cargo #3 -- get egg -> in Cage
      In Cage -- shoot chain                       25 = 400
           leave cage - #4, W -> room U -> Central
[The EGG, GUN, and this whole section is unnecessary for game completion.]
      29 = Time                                    Yell = 40
           (have suit)
           TYPE 8480 ON KEYPAD -> panel opens, get black box  30=470
           (Number is on screen in Computer Room)
[TYPE XXXX, where XXXX is the random 4 digits you got from SHIPS'S SYTEMS 
screen 4, input it exactly like: "TYPE 7654" because anything else confuses 
the game. This box is how you return to your present time.]

           PRESS SWITCH -> fall into chair       
[or SIT IN CHAIR]
        
           TURN GOLD DIAL TO 2171 THEN PRESS TAN BUTTON       55=525
[or TURN GOLD TO 2171, PUSH BUTTON. You want to have the SUIT in your inventory,
to have healed yourself in the TUBE, and to have eaten the PILL, before this 
point. Or you'll be doing a lot of backtracking.]
 
Time Travel:
Wooden Bridge:
   N to broken end, find object, get -> quarter              10=535
   S to deserted road (W-crater floor, lose possessions)
     Crater's Edge--find wire.                               20=555
[To leave CRATER FLOOR, go E multiple times until it works. Like 1 in 5 times it
 will work, other times you will just wander around. Confusing puzzle.]
      (creature steals, but find again)
      (you need the wire, so find it before going on)
    Go S on Separate Path to Front of Building                30=585
[To find this building, go E on DESERTED ROAD until the description subtlely 
changes. About 10X]

Front of Building:
   S -> Lobby, S -> Office, W -> Office, W -> Office, 
   NW -> Office with Updraft, D -> Basement                  110=695
[A classic text adventure game maze. Either follow directions exactly, or 
drop different items on the floor to map it out]

Basement;
   Open door with wire, look inside cabinet, 
[PICK LOCK WITH WIRE. GET ALL]
   get helmet (drop flashlight), get key, go S (Office with Updraft)
   SE, SW (ladder), U -> Roof
Roof:
   Enter vehicle (heed warning)
   Insert key in keyhole, wear helmet then push white button 60=755

[You can fly this craft around for fun, or pull the lever or entertaining 
suicide, but the floating city is the only location of note. If you pull 
the black boxswitch after flying the shuttle, but before encountering the 
creature, you can return to the top of the office building and the shuttle 
will have resetted to its original spot.]
   U, U -> Above the Clouds  
Above the Clouds:                                            45=800
   E -> Outside City (no sign of life), 
   N -> Outside City (large flat area),                      30=830
   Land shuttle
Inside Shuttle:
   Take off helmet then wear suit  (don't need helmet, key, flashlight)
   Exit (alternate: N or leave shuttle)
City:
   Landing Bay SE -> Power Plant E -> Dark Corridor, E -> Dark Corridor
   S -> Dark Corridor (bad smell), S -> Food Supply and creature

[The creature is the only thing of note in the dark corridors. You might want to
save the game before entering the room with the creature, because once the 
creature is following you, there is very little you can do without dying.]

   PULL SWITCH (black box) 
Ship:
   You should be transported back to the ship TIME CHAMBER and the
   creature will follow very soon.  I am not sure if you need to
   shoot the creature in the chamber, but I did.
   SHOOT CREATURE WITH GUN, STAND (now move fast)
[Shooting the creature is optional, what's important is that you kill the 
creature with radiation. -Z]
   W -> Central, NW -> Engine Room (wearing suit), PRESS RED BUTTON
   (the monster melts from the radiation)                    65=895
   GET TAPE                                                  10=905
Central:                                                     10=915
Bridge:
   examine controls -> slot, put tape in slot then examine screen
   which will show a year (1933)                              5=920
Time Chamber:
   Sit in chair, set gold dial to (year), press tan button  80=1000
Congratulations! You have won.                              
[You can cheat and jump to 1933 as soon as you enter the UFO and win the game 
with a very low score. You will still be considered a master adventure.
Cheesy, but part of the beauty of old school adventure games. I want to thank 
Scott Miller for making a game that, while not perfect, was good enough to 
inspire me to finish what my 15 year old self 
couldn't do. And A. De Lisle for making the original walkthrough - Zizz]
------------------------------------------------------------------
I would give you my phone number, but I have moved since then.
I still live here these many years later.  The building where I
lived in 1933 was done in by the 1989 earthquake, but I had moved
many years before that and my present home was not damaged.        
=================================================================

