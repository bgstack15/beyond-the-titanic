#!/bin/sh
# File: /usr/bin/beyond-the-titanic
# Author: bgstack15
# Startdate: 2019-03-31 17:55
# Title: Beyond the Titanic
# Package;
# Purpose: Runs BEYOND from the correct location
# History:
# Usage: Normally called by /usr/share/applications/beyond-the-titanic.desktop
# Reference:
#    https://stackoverflow.com/questions/2924697/how-does-one-output-bold-text-in-bash
# Improve:
# Documentation:
# Dependencies:
#    file /etc/default/beyond-the-titanic
#    tput

# load variables from defaults file
test -r "/etc/default/beyond-the-titanic" && . /etc/default/beyond-the-titanic 1>/dev/null 2>&1
test -z "${BEYOND_SRC_DIR}" && BEYOND_SRC_DIR=/usr/share/beyond-the-titanic
test -z "${BEYOND_PLAY_DIR}" && BEYOND_PLAY_DIR=~/.local/share/beyond-the-titanic

# prepare play directory, because the game needs write access to the data files, as well as to the directory for saving the game
mkdir -p "${BEYOND_PLAY_DIR}" 2>/dev/null
chmod 0700 "${BEYOND_PLAY_DIR}"
cp -pr "${BEYOND_SRC_DIR}"/* "${BEYOND_PLAY_DIR}"
chmod 0600 "${BEYOND_PLAY_DIR}"/*
chmod 0700 "${BEYOND_PLAY_DIR}/BEYOND"

# warn about screen size if not correct
if test -z "${BEYOND_SILENT}" ;
then
   ___cols="$( tput cols 2>/dev/null )"
   ___lines="$( tput lines 2>/dev/null )"
   if test "${___cols}" != "80" || test "${___lines}" != "24" ;
   then
      printf "The game recommends using a terminal size of exactly 80 columns by 24 lines. Set BEYOND_SILENT=1 to suppress this message.\nPress enter to continue. "
      read ___foo ;
   fi
fi

# start game
cd "${BEYOND_PLAY_DIR}"
./BEYOND

# return terminal text to normal style
tput sgr0
